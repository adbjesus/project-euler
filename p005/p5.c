#include <stdio.h>
#include <stdlib.h>

int main(void){
	int n1,n2,res,i;
	printf("Lower limit: ");
	scanf("%d",&n1);
	printf("Upper limit: ");
	scanf("%d",&n2);
	/*We start on 0 and go up or down until we find the result by the upper limit*/
	for(res=n2;;res=res+n2){
		for(i=n2/2;i<n2;i++){
			if(res%i!=0) break;
		}
		if(i==n2) break;
	}

	printf("Res: %d\n",res);
	
	return 0;
}

