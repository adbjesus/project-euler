from math import sqrt 

n = input()
primes = [2,3]

def is_prime(p):
	l=sqrt(p)
	for pr in primes:
		if(p%pr==0): return False
		if(pr>l): return True 
	return True

def get_next_prime():
	p = primes[-1]+2
	while(is_prime(p)==False):
		p=p+2
	primes.append(p)

while(primes[-1]<n):
	get_next_prime()

print sum(primes)-primes[-1]
