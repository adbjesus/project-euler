#include <stdio.h>
#include <stdlib.h>

#define MAX 1000

int X,Y,N;
int max;
int grid[MAX][MAX];

void right(int x,int y){
	int s,i;
	if((x-1)+N>=X){
		return;
	}
	for(i=0,s=1;i<N;i++){
		s=s*grid[y][x+i];
	}
	if(s>max) max=s;
}

void down(int x,int y){
	int s,i;
	if((y-1)+N>=Y){
		return;
	}
	for(i=0,s=1;i<N;i++){
		s=s*grid[y+i][x];
	}
	if(s>max) max=s;
}

void right_down(int x,int y){
	int s,i;
	if((y-1)+N>=Y || (x-1)+N>=X){
		return;
	}
	for(i=0,s=1;i<N;i++){
		s=s*grid[y+i][x+i];
	}
	if(s>max) max=s;
}

void right_top(int x,int y){
	int s,i;
	if((y+1)-N<0 || (x-1)+N>=X){
		return;
	}
	for(i=0,s=1;i<N;i++){
		s=s*grid[y-i][x+i];
	}
	if(s>max) max=s;
}

int main(void){
	int x,y;
	scanf("%d %d %d",&Y,&X,&N);
	for(y=0;y<Y;y++){
		for(x=0;x<X;x++){
			scanf("%d",&grid[y][x]);
		}
	}
	for(x=0;x<X;x++){
		for(y=0;y<Y;y++){
			right(x,y);
			down(x,y);
			right_down(x,y);
			right_top(x,y);
		}
	}
	printf("%d\n",max);
	return 0;
}			
		
