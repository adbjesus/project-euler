primes=[2,3]

def is_prime(p):
	for pr in primes:
		if(p%pr==0): return False
	return True

def get_next_prime():
	p = primes[-1]
	while(is_prime(p)==False):
		p=p+2
	primes.append(p)
		
n=input()
while(len(primes)<n):
	get_next_prime()

print primes[n-1]
